const app = Elm.Main.fullscreen({
    clients: localStorage.getItem("tooty.clients") || "[]",
    registration: JSON.parse(localStorage.getItem("tooty.registration"))
});

app.ports.saveClients.subscribe(json => {
    localStorage.setItem("tooty.clients", json);
});

app.ports.saveRegistration.subscribe(json => {
    localStorage.setItem("tooty.registration", json);
});

app.ports.deleteRegistration.subscribe(json => {
    localStorage.removeItem("tooty.registration");
});

app.ports.setStatus.subscribe(function (data) {
    var element = document.getElementById(data.id);
    if (element) {
        element.value = data.status;
        // Reset cursor at the end
        element.focus();
    }
});

app.ports.scrollIntoView.subscribe(function (id) {
    requestAnimationFrame(function () {
        var element = document.getElementById(id);
        if (element) {
            element.scrollIntoView(false);
        }
    });
});

app.ports.uploadMedia.subscribe(data => {
    const files = Array.from(document.getElementById(data.id).files);
    const formData = new FormData();
    formData.append("file", files[0]);
    fetch(data.url, {
        method: "POST",
        headers: { Authorization: "Bearer " + data.token },
        body: formData,
    })
        .catch(err => app.ports.uploadError.send(err.message))
        .then(response => response.text())
        .then(app.ports.uploadSuccess.send);
});

function notify(data) {
    const notif = new Notification(data.title, {
        icon: data.icon,
        body: data.body,
    });
    notif.onclick = () => location.hash = data.clickUrl;
}

app.ports.notify.subscribe(data => {
    if (Notification.permission === "granted") {
        notify(data);
    } else if (Notification.permission !== "denied") {
        Notification.requestPermission(permission => {
            if (permission === "granted") {
                notify(data);
            }
        });
    }
});

// Added by Julien

function moveContainer() {
    const container = document.getElementsByClassName('container-fluid');
    if (container.length === 0) {
        setTimeout(function () {
            moveContainer();
        }, 1000);
    } else {
        document.getElementById('app-content').appendChild(container[0]);
        // to make NC menu hide extra items (weirdly inhibited by tooty)
        window.dispatchEvent(new Event('resize'));
    }
}

document.addEventListener('DOMContentLoaded', function(event) {
    moveContainer();
});