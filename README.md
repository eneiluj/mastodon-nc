# Mastodon Nextcloud application

Mastodon-nc is a Mastodon client based to [Tooty](https://github.com/n1k0/tooty) and able to connect to any Mastodon instance.

## Releases

[Here](https://gitlab.com/eneiluj/mastodon-nc/wikis/home#releases) are the Mastodon releases.

