<?php
/**
 * Nextcloud - Mastodon
 *
 *
 * @author
 *
 * @copyright
 */

namespace OCA\Mastodon\AppInfo;



use OCP\IContainer;

use OCP\AppFramework\App;
use OCP\AppFramework\IAppContainer;

use OCA\Mastodon\Controller\PageController;

/**
 * Class Application
 *
 * @package OCA\Mastodon\AppInfo
 */
class Application extends App {

    /**
     * Constructor
     *
     * @param array $urlParams
     */
    public function __construct(array $urlParams = []) {
        parent::__construct('mastodon', $urlParams);

        $container = $this->getContainer();
    }

}

